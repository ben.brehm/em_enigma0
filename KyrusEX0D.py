import math
from math import sqrt
import sympy
from sympy import *
from sympy import substitution
from sympy import Symbol
from sympy.parsing.sympy_parser import parse_expr
import time
from collections import Counter, OrderedDict

start_time = time.time()


# Depreciated dirty solution

# startval = 2
# upperbound = 20
# a = startval
# b = startval
# c = startval
# d = startval
# e = startval
# f = startval
# g = startval

# def enigma0():
#     for a in range(2, upperbound):
#         for b in range(2, upperbound):
#             for c in range(2, upperbound):
#                 # for d in range(2,upperbound):
#                 for e in range(2, upperbound):
#
#                     d = round((7 * 5 ** (2 / 3) * 2 ** (1 / 3)) / (
#                         (a ** (1 / 3) * c ** (2 / 3))))
#                     f = (39 * sqrt(22)) / ((sqrt(a) * b * (sqrt(e))))
#                     # for f in range(2,20):
#                     g = 5100 / ((a ** 2) * b * (c ** 2))
#                     if ((a ** 2) * b * (c ** 2) * g == 5100 and a * (
#                             b ** 2) * e * (f ** 2) == 33462 and a * (
#                             c ** 2) * (d ** 3) == 17150 and (a ** 3) * (
#                             b ** 3) * c * d * (e ** 2) == 914760):
#                         print(a, b, c, d, e, f, g)
#                         print("time elapsed : {:f}s".format(
#                             time.time() - start_time))
#                         return

# Function for finding the variables in input equations
def common(str1, str2):
    # convert both strings into counter dictionary
    dict1 = Counter(str1)
    dict2 = Counter(str2)

    # take intersection of these dictionaries
    commonDict = dict1 & dict2

    if len(commonDict) == 0:
        print - 1
        return

    # get a list of common elements
    commonChars = list(commonDict.elements())

    # sort list in ascending order to print resultant
    # string on alphabetical order
    commonChars = sorted(commonChars)

    binresult = ''.join(commonChars)
    return binresult


# Function to remove all duplicates from the equation list variables
def removeDupWithoutOrder(str):
    return "".join(set(str))


# Sort the variables from the equation list
def sortString(str):
    return ''.join(sorted(str))


# Getter for variables from input equations
def getVariables():
    for x in range(len(equationslist)):
        equationslist[x] = equationslist[x].rstrip("\n")

    variables = ('abcdefghijklmnopqrstuvwxyz')
    varsresult = ''
    # bin = common(variables, equationslist[0])
    # print(bin)

    for x in range(len(equationslist)):
        commonresult = common(variables, equationslist[x])
        varsresult = varsresult + commonresult

    # clean up the string
    varsresult = removeDupWithoutOrder(varsresult)
    varsresult = sortString(varsresult)

    return varsresult


# Getter for constraints from input equations
def getConstraints():
    constraintresult = [None] * len(equationslist)

    for x in range(len(equationslist)):
        index = equationslist[x].find('-')
        constraintresult[x] = equationslist[x][
                              (index + 1):len(equationslist[x])]

    return constraintresult


if __name__ == "__main__":

    # open input
    equationsfile = open("equations.txt", "r")
    equationslist = equationsfile.readlines()

    # parse the input equations for evaluation
    parselist = [None] * len(equationslist)
    for x in range(len(equationslist)):
        parselist[x] = parse_expr(equationslist[x])

    print("Sympy parsed input equations: ", parselist)

    # retrieve variables and constraints from input
    varsresult = getVariables()
    constraintresult = getConstraints()

    # dynamically set sympy symbols according to retrieved variables
    vardict = {}
    for x in range(len(varsresult)):
        vardict[x] = sympy.symbols(varsresult[x])

    print("Variables from input equations: ", varsresult)
    print("Constraints from input equations: ", constraintresult)
    print("Sympy symbol dictionary: ", vardict)

    # convert string constraints to ints
    constraintints = [None] * len(equationslist)
    for x in range(len(equationslist)):
        constraintints[x] = int(constraintresult[x])
    print("Constraint integer list: ", constraintints)

    # retrieve integer factors of constraints
    # a list of dictionaries is returned
    constraint_integer_factors = [None] * len(constraintints)
    for x in range(len(constraintints)):
        constraint_integer_factors[x] = sympy.factorint(constraintints[x])

    # get all the keys of factor dictionary
    # the keys are the solutions
    constraint_integer_factors_keys = [None]
    for x in range(len(constraint_integer_factors)):
        constraint_integer_factors_keys.extend(
            (constraint_integer_factors[x].keys()))

    # get rid of the empty index
    del constraint_integer_factors_keys[0]

    # delete duplicate factors and sort from smallest to largest
    constraint_integer_factors_keys_sorted = []
    for x in constraint_integer_factors_keys:
        if x not in constraint_integer_factors_keys_sorted:
            constraint_integer_factors_keys_sorted.append(x)
    constraint_integer_factors_keys_sorted.sort()

    print("Integer factors of constraints: ",
          constraint_integer_factors_keys_sorted)

    expression = parselist[0].subs(vardict[0], 2)
    expression = expression.subs(vardict[1], 3)
    expression = expression.subs(vardict[2], 5)
    expression = expression.subs(vardict[6], 17)
    # cond = (expression == 0)
    # print(cond)

    # Tunning the key factors through all the parsed
    # equations in order to verify the system of equations has a positive
    # integer solution greater than 1
    # The inner loop will iterate through each parsed equation.
    # The outer loop will iterate all variables through all the retrieved
    # key factors
    # If the constraint list supplied doesn't work, pop it and try again
    result_list = [None] * len(parselist)
    # print(result_list)
    for x in range(len(equationslist)):
        result_list[x] = parselist[x]
        for y in range(len(constraint_integer_factors_keys_sorted)):
            for z in range(len(constraint_integer_factors_keys_sorted)):
                result_list[x] = result_list[x].subs(vardict[z],
                                    constraint_integer_factors_keys_sorted[
                                                         z])
            if (result_list[x] != 0):
                constraint_integer_factors_keys_sorted.pop()
            else:
                break

    # print(result_list)
    if (all(result_list) == 0):
        print("Solutions: ", constraint_integer_factors_keys_sorted)
    else:
        print("No common integer solutions > 1 for: ", parselist)

    print("time elapsed : {:f}s".format(time.time() - start_time))
    # enigma0()

    equationsfile.close()
